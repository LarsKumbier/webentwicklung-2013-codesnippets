$( document ).ready(function() {
	// Folding of an element
	$( "a#fold" ).click(function( event ) {
		event.preventDefault();
		
		if ( $("#lorem").attr('class') == 'foldOpen' ) {
			$( "#lorem" ).toggleClass('foldOpen', false);
			$( "#lorem" ).toggleClass('foldClosed', true);
			$( "#lorem" ).slideUp( 1250 );
			$( "a#fold" ).text("Fold Down");
		} else {
			$( "#lorem" ).toggleClass('foldOpen', true);
			$( "#lorem" ).toggleClass('foldClosed', false);
			$( "#lorem" ).slideDown( 1250 );
			$( "a#fold" ).text("Fold Up");
		}
		
		console.log( "link was clicked" );
	});
	
	
	// Autocomplete
	$('#autocomplete').autocomplete({
    serviceUrl: 'countries.php',
		deferRequestBy: 500,
    onSelect: function (suggestion) {
        console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
    }
	});
});