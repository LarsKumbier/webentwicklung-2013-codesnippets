﻿<?php
	$msg = null;
	$action = $_REQUEST['action'];
	
	// * Schaut nach, ob eine Session existiert (Cookie oder $_GET)
	// * falls ja, lädt es die Daten aus dem Speicher in $_SESSION
	// * falls nein, legt es einen neue Session an und versucht, auf dem
	//   Browser ein Cookie zu setzen
	session_start();
	
	switch ($action) {
		case 'reload':
			$msg = 'Seite wird neu geladen';
			break;
			
		case 'destroy':
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000, $params["path"],
				$params["domain"], $params["secure"], $params["httponly"]
			);
			session_destroy();
			$msg = 'Session wurde zerstört:';
			break;
			
		case 'renew':
			$previous = session_id();
			session_regenerate_id(true);
			$new = session_id();
			$msg = "Alte ID: $previous<br />Neue ID: $new";
			break;
			
		case 'add':
			$key = $_REQUEST['key'];
			$value = $_REQUEST['value'];
			$_SESSION[$key] = $value;
			$msg = "Zur Session hinzugefügt: $key => $value";
			break;
	}

	
?>
<DOCTYPE html>
<html>
	<head>
		<title>Session Test</title>
	</head>
	<body>
		<h1>Session Test</h1>
		
		<p style="background-color: orange">
			<?= $msg ?>
		</p>
		
		<table border="1">
			<tr>
				<th>Session Id:</th>
				<td colspan="2"><?= session_id(); ?></td>
			</tr>
			<tr>
				<th rowspan="<?php echo count($_SESSION); ?>">Session Data:</th>
				<?php foreach ($_SESSION as $key => $value) : ?>
				<td><?=$key?></td>
				<td><?=$value?></td>
			</tr>
			<tr>
				<?php endforeach; ?>
			</tr>
		</table>
		
		<h2>Optionen</h2>
		<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
			<p>
				<button name="action" value="reload">Lade Seite neu</button>
				<button name="action" value="renew">Neue Identität</button>
				<button name="action" value="destroy">Beende aktuelle Session</button>
			</p>
		</form>
			
		<h3>Neue Werte anlegen:</h3>
		<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
			<label for="key">Schlüssel:</label>
			<input type="text" size=32 name="key">
			<br>
			<label for="value">Wert:</label>
			<input type="text" size=32 name="value">
			<br>
			<button type="submit" name="action" value="add">Hinzufügen</button>
		</form>
	</body>
</html>
